package com.example.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplus.model.domain.SysAdmin;
import com.example.mybatisplus.mapper.SysAdminMapper;
import com.example.mybatisplus.service.SysAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxq
 * @since 2023-01-02
 */
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin> implements SysAdminService {
    @Autowired
    private SysAdminMapper sysAdminMapper;
    @Override
    public SysAdmin login(SysAdmin sysAdmin) {
        QueryWrapper<SysAdmin> sysAdminQueryWrapper = new QueryWrapper<>();
        sysAdminQueryWrapper.eq("adminSn", sysAdmin.getAdminSn()).eq("password", sysAdmin.getPassword())
                .eq("role_id", 1);
        SysAdmin one = sysAdminMapper.selectOne(sysAdminQueryWrapper);
        return one;
    }
}
